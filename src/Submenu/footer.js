import React from 'react';
import styled from 'styled-components';
import {Circle,Icon} from './options';

import check from '../images/ok.png';
import cancel from '../images/error.png';
import reset from '../images/refresh.png';


const Container = styled.div`
    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:row;
   
`;

const ContainerV2 = styled.div`
    display:flex;
    flex-direction:row;
    justify-content:center;
    align-items:center;
    margin-top : 15px;
    padding-top:15px;
    padding-bottom:15px; 
`;

const Item = ({v,onClick}) =>
        {return (
                <Circle visible={true} style={{ marginLeft:15,height:60,width:60}} onClick={onClick}>
                    <Icon visible={true} src={v} style={{height:60,width:60}}></Icon>
                </Circle>
    )};

const App = ({version,onReset,onCancel,onAccept}) =>{

    const handleReset = () => {
        console.log("vamos a resetear" );
        if(typeof onReset === 'function') onReset()
    }

    const handleOk = () => {
        console.log("vamos a Aceptar" );
        if(typeof onAccept === 'function') onAccept()
    }

    const handleCancel = () => {
        console.log("vamos a Cancelar" );
        if(typeof onCancel === 'function') onCancel()
    }
    

    if(version === 2){
        let data = [
            {icon:cancel, onClick:handleCancel},
            {icon:check, onClick:handleOk}
        ]
        return(
            <Container >
                {data.map((v,i) =><Item v={v.icon} onClick={v.onClick}/>)}
            </Container>
        );
    }else if(version === 1){
        let data = [
            {icon:cancel, onClick:handleCancel},
            {icon:reset, onClick: handleReset },
            {icon:check, onClick: handleOk}
        ]
        return(
            <ContainerV2  style={{borderTop: '1px solid #ffffff80'}}>
                {data.map((v,i) =><Item v={v.icon} onClick={v.onClick}/> )}
            </ContainerV2>
        );
    }else{
        return;
    }
    
}

export default App;