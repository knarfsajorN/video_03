import React from 'react';
import styled from 'styled-components';
import Options from './options';
import Footer from './footer';

//import check from '../images/ok.png'
//import cancel from '../images/error.png'


const Container = styled.div`
    margin-top:15px;
    background-color: ${props=>props.color}; //:#21d0d0;
    border-radius:15px;
    //height:200px;
    width: 400px;
    padding-top:15px;

    //opacity:0.8;
`;

const App = (params) =>{
    /*
    * color: string;
    * options: string[];
    */

    const handleChange = (value) => {
        if(typeof params.onChange === 'function') params.onChange(value,params.id)
        console.log(value)
    }
    
    return(
        <Container color={params.color}>
            { params.options.map((v,i) => <Options key={i} {...v} onChange={handleChange} />)}
            <Footer version={params.footer} /*onReset onCancel onAccept*/ />
        </Container >
    );
}

export default App;