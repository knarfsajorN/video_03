
import React, { useState } from 'react';
import styled from 'styled-components';

import Card from './Card';
import Submenu from './Submenu';
import Options from './Submenu/options';

const Container = styled.div`

`


const App =() => {
  const [data, setData]  = useState([
    {
      id:1,
      color: "#21d0d0",
      options : [
        {title: "PRICE LOW TO HIGHT",state:false},
        {title: "PRICE HIGH TO LOW",state:false},
        {title: "POPULARITY",state:true},
      ],
      footer:2
    },
    {
      id:2,
      color: "#ff7745",
      options : [
        {title: "1UP NUTRIRION",state:true},
        {title: "ASITIS",state:true},
        {title: "AVVATAR",state:true},
        {title: "BIG MUSCLES",state:true},
        {title: "BPI SPORTS",state:true},
        {title: "BSN",state:true},
        {title: "CELLUCOR",state:true},
        {title: "DOMIN8R",state:true},
        {title: "DYMATIZE",state:true},
      ],
      footer:1
    }
  ])

 
   
  const handleChange =  (value,id) => {

    const reemplace = (raw, newList) => {
      const copy = [...raw];
      const targetIndex=copy.findIndex(f=>f.id === id);
      if (targetIndex > -1) {
        copy[targetIndex].options = newList;
      }
    }

    const reemplaceOptions = (raw, newOptions) => {
      const copy = [...raw];
      const targetIndex=copy.findIndex(f=>f.id === id);
      if (targetIndex > -1) {
        copy[targetIndex].options = newOptions;
      }
    }

    // console.log(value, id);
    const match = data.filter(el=> el.id == id)[0]
    // console.log(match)
    const prev = match.options.filter(el=>el.title !== value)
    const act = match.options.filter(el=>el.title === value)[0]
    prev.push({value,state:!act.state})
    //match.options = prev
    // const raw = data.filter(el=> el.id !== id);
    // raw.push(match);
    // setData(raw);
    const copy =[...data];
    const targetIndex = copy.findIndex(f=>f.id = id);
    if (targetIndex > -1) {
      copy[targetIndex].options = {title:value,state:!act.state};
      setData(copy)
      //return updateObject(state,{data:copy})
    }
  }

  return (
    <Container>
      <Card/> 
      {/* <Submenu color={"#21d0d0"}/> */}
      {data.map((v,i) => <Submenu {...v} onChange={handleChange}/>) }
      {/* <Submenu color={"#ff7745"}/> */}
    </Container>
  );
}

export default App;
